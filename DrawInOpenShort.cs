﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace HiTouch_Designer
{
    public partial class DrawInOpenShort : Form
    {
        public coordinate coordinate_component; //For Draw Line
        public int TXNum = 0;
        public int RXNum = 0;
        private Color[] colorArray = { Color.Transparent, Color.Red, Color.Blue,Color.DarkGreen, Color.Magenta, Color.BlueViolet, Color.RoyalBlue, 
			Color.DeepSkyBlue, Color.SpringGreen, Color.Yellow, Color.DarkOrange };
        
        public DrawInOpenShort()
        {
            InitializeComponent();
            //Init();
        }
        public void Init(int tx_num, int rx_num, int pt_num)
        {
            TXNum = tx_num;
            RXNum = rx_num;

            if (coordinate_component != null)
                coordinate_component.Clean_Panel();
            coordinate_component = new coordinate(pt_num, 0, this.pictureBox_Draw, null, null);
        }
        public void setSizeAndLocation(int x, int y, int width, int height)
        {
            this.Location = new Point(x, y);
            setSize(width, height);
        }
        public void changeLocation(int state)
        {
            int x = this.Location.X;
            int y = this.Location.Y;
            if (state == 0)
                y += 5;
            else if (state == 1)
                y -= 5;
            else if (state == 2)
                x -= 5;
            else if (state == 3)
                x += 5;
            this.Location = new Point(x, y);
        }
        public void changeLengthForTrianglePattern()
        {
            int x = this.Width;
            int y = this.Height;
            x = 480;
            y = 800;
            this.Width = x;
            this.Height = y;
        }
        public void changeLength(int state)
        {
            int x = this.Width;
            int y = this.Height;
            if (state == 0)
                y += 5;
            else if (state == 1)
                y -= 5;
            else if (state == 2)
                x -= 5;
            else if (state == 3)
                x += 5;
            this.Width = x;
            this.Height = y;
        }

        public void setSize(int width, int height)
        {
            this.Width = width;
            this.Height = height;
            this.panelCanvas.SetBounds(0, 0, width, height);
            this.pictureBox_Draw.SetBounds(0, 0, width, height);
        }

        private void DrawInOpenShort_FormClosed(object sender, FormClosedEventArgs e)
        {
        }

        private void DrawInOpenShort_FormClosing(object sender, FormClosingEventArgs e)
        {
            //this.Hide();
            //return;
            //e.Cancel = true; 
        }

        private void DrawInOpenShort_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F3)
            {
                this.Visible = !this.Visible;
            }
        }
    }
}
